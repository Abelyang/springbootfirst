package com.example.base.dao;

import java.util.List;
import java.util.Map;

import com.example.base.model.User;
import com.example.config.MyBatisRepository;

@MyBatisRepository
public interface UserDao {
	public List<User> getList(Map<String,Object> map);
}
